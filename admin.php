<?php

require(__DIR__ .  '/models/Films.php');

$r = new Films();
$r->select(['id', 'name', 'year']);

$type = 1;

if (isset($_GET['sort'])) {
    $sort = $_GET['sort'];
    $sort['key'] = trim($sort['key']);

    $type = $sort['type'] ? 0 : 1;
    $t = $sort['type'] ? "ASC" : "DESC";
    $res = $r->order($sort['key'], $t)->all();
} else {
    $res = $r->all();
}
require(__DIR__ .  '/head.php');
?>
<a href="form.php" title="Add">
       <span class="glyphicon glyphicon-pencil"></span> Add
</a> 
<table class="table table-striped">
    <thead>
        <tr>
            <td><a href="?sort[key]=name&sort[type]=<?= $type ?>">Name</a></td>
            <td><a href="?sort[key]=year&sort[type]=<?= $type ?>">Year</a></td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($res as $item): ?>
            <tr>
                <?php foreach ($item as $i): ?>
                    <td><?= $i ?></td>
                <?php endforeach; ?>                                
                <td>
                    <a href="form.php/?id=<?= $item['id'] ?>" title="Update">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a> 
                    <a href="form.php/?id='<?= $item['id'] ?>'" title="Delete">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php require(__DIR__ .  '/footer.php');?>
