<?php
require(__DIR__ .  '/models/Films.php');

$r = new Films();

$type = 1;

if (isset($_GET['id'])) {
    $id = trim($_GET['id']);
    $res = $r->select()->where("id = " . $id)->all();
    $res = $res[0];
}
if(isset($_POST['Films'])){
    $films = $_POST['Films'];
    $r->key = 'name, year, isActive';
    $r->insert($films);
}

require(__DIR__ .  '/head.php');
?>
<a href="admin.php" title="Admin">Admin</a>
<form method="POST">  
    <input type="hidden" name="Films[id]" value="<?=$res['id']?>">
    <div class="form-group">
        <label class="control-label" for="name">Name</label>
        <input type="text" id="name" class="form-control" name="Films[name]" maxlength="50"  value="<?=isset($res['name']) ? $res['name'] : ""?>">                    
    </div>
    
    <div class="form-group">
        <label class="control-label" for="year">Year</label>
        <select id="year" class="form-control" name="Films[year]">
            <?php $y = isset($res['year']) ? $res['year'] : date("Y");?>
            <?php for($i = min(date("Y") - 10,$y-10); $i < max(date("Y") + 10,$y+10); $i++) :?>
                <option <?= (isset($res['year']) && $res['year'] == $i) ? 'selected="selected"' : ""?>><?=$i?></option>            
            <?php endfor;?>
            
        </select>    
    </div>
    
    <div class="form-group">
        <input type="hidden" name="Films[isActive]" value="0">
        <label>Is Active
            <input type="checkbox" id="films-isactive" name="Films[isActive]" value="1" <?=isset($res['isActive']) ? ($res['isActive'] == 1) ? 'checked="checked"' : "" : 'checked="checked"' ?>>                         
        </label>
    </div>
    
    <div class="form-group">
        <button type="submit" class="btn btn-success">Push</button>
    </div>
</form>
<?php require(__DIR__ .  '/footer.php');?>