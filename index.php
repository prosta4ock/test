<?php
require(__DIR__ .  '/models/Films.php');

$r = new Films();
$r->select(['name', 'year'])->where("isActive = 1");

$type = 1;

if (isset($_GET['sort'])) {
    $sort = $_GET['sort'];
    $sort['key'] = trim($sort['key']);

    $type = $sort['type'] ? 0 : 1;
    $t = $sort['type'] ? "ASC" : "DESC";
    $res = $r->order($sort['key'], $t)->all();
} else {
    $res = $r->all();
}

require(__DIR__ . '/head.php');
?>

<table class="table table-striped">
    <thead>
        <tr>
            <td><a href="?sort[key]=name&sort[type]=<?= $type ?>">Name</a></td>
            <td><a href="?sort[key]=year&sort[type]=<?= $type ?>">Year</a></td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($res as $item): ?>
            <tr>
                <?php foreach ($item as $i): ?>
                    <td><?= $i ?></td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php require(__DIR__ .  '/footer.php'); ?>