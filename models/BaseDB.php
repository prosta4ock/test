<?php

require(__DIR__ . '/../config/db.php');

/**
 * Description of BaseDB
 *
 * @author Artem
 */
class BaseDB {

    public $query = ['select'=>'', 'from' => '', 'join'=>'', 'where'=>'', 'group by'=>'', 'order by'=>''];
    public $key;
    protected $_from;

    public function __construct() {
        $this->_from = mb_strtolower(get_class($this));
    }

    public function select(array $select = null) {
        $select = (!$select) ? '*' : implode(",", $select);
        $this->query['select'] = $select;
        $this->query['from'] = $this->_from;
        return $this;
    }

    public function where($where) {
        $this->query['where'] = $where;

        return $this;
    }

    public function order($k, $t){
        $this->query['order by'] = $k ." ".$t;
        
        return $this;
    }
    
    public function all() {
        $ret = [];
        $query = '';
        foreach($this->query as $k=>$n){
            if(trim($n) != '')
                $query .= $k ." ".$n." ";
        }
        
        $res = mysqli_query(start(), $query);
        while ($row = mysqli_fetch_assoc($res)) {
            $ret[] = $row;
        }

        return $ret;
    }

    public function insert($insert) {
        if(trim($insert['id']) == ''){
            unset($insert['id']);
            $sql = "INSERT INTO `{$this->_from}`({$this->key}) VALUES ";
            $sql .= $this->res($insert);
        }
        else{
            $sql = "UPDATE `{$this->_from}` SET ";
            foreach($insert as $k=>$i){
                $i = is_numeric($i) ? $i : "'".$i."'";
                $sql .= "`".$k."`=".$i;
                
                if(end($insert) != $i)
                    $sql.= ", ";
            }
            $sql .= " WHERE id=".$insert['id'];
        }
        
        if (!mysqli_query(start(),  $sql)) {
            return false;
        } else {
            return true;
        }
    }
    
    protected function res($arr, $_insert = "("){
        foreach($arr as $i){    
            if(is_numeric($i)){
                $_insert .= $i;
            }
            else{
                $_insert .= '"'.$i.'"';
            }
            if(end($arr) != $i)
                $_insert .= ',';
        }
        $_insert .= ")";
        
        return $_insert;
    }
}